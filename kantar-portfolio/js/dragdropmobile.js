function openEgypt() {
    document.getElementById("ddEgypt").style.width = "100%";

   $('#ddEgypt .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddEgypt').style.width = "0%";
        $('#egyptRate .ddRate').html(text);
        $('#egyptRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeEgypt() {
    document.getElementById("ddEgypt").style.width = "0%";
}
function openFrance() {
    document.getElementById("ddFrance").style.width = "100%";
    $('#ddFrance .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddFrance').style.width = "0%";
        $('#franceRate .ddRate').html(text);
        $('#franceRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeFrance() {
    document.getElementById("ddFrance").style.width = "0%";
}
function openIndia() {
    document.getElementById("ddIndia").style.width = "100%";
    $('#ddIndia .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddIndia').style.width = "0%";
        $('#indiaRate .ddRate').html(text);
        $('#indiaRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeIndia() {
    document.getElementById("ddIndia").style.width = "0%";
}
function openUs() {
    document.getElementById("ddUs").style.width = "100%";
    $('#ddUs .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddUs').style.width = "0%";
        $('#usRate .ddRate').html(text);
        $('#usRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeUs() {
    document.getElementById("ddUs").style.width = "0%";
}
function openAus() {
    document.getElementById("ddAus").style.width = "100%";
    $('#ddAus .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddAus').style.width = "0%";
        $('#ausRate .ddRate').html(text);
        $('#ausRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeAus() {
    document.getElementById("ddAus").style.width = "0%";
}
function openItaly() {
    document.getElementById("ddItaly").style.width = "100%";
    $('#ddItaly .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddItaly').style.width = "0%";
        $('#italyRate .ddRate').html(text);
        $('#italyRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeItaly() {
    document.getElementById("ddItaly").style.width = "0%";
}
function openRussia() {
    document.getElementById("ddRussia").style.width = "100%";
    $('#ddRussia .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddRussia').style.width = "0%";
        $('#russRate .ddRate').html(text);
        $('#russRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeRussia() {
    document.getElementById("ddRussia").style.width = "0%";
}
function openNeth() {
    document.getElementById("ddNeth").style.width = "100%";
    $('#ddNeth .overlay-content a').click(function() {
        var text = $(this).text();
        document.getElementById('ddNeth').style.width = "0%";
        $('#nethRate .ddRate').html(text);
        $('#nethRate .ddRate').prepend('<i style="color:#0096AE" class="fas fa-check"></i> ');
   });
}
function closeNeth() {
    document.getElementById("ddNeth").style.width = "0%";
}