// Custom Javascript
//Navigation
$('.nav a[data-toggle="tab"]').on('click', function(){
  $('.navbar-toggle').click() //bootstrap 3.x by Richard
});
//Responsive switch
$(function() {
  var desktop = document.getElementById('desktop');
  var tablet = document.getElementById('tablet');
  var phone = document.getElementById('phone');
  desktop.onclick = function() {
    $('.container').css('width', '75%');
    $('.multiImageZoom .col-md-6').removeClass('col-md-6').addClass('col-md-3');
    $('.multiImageZoom .col-md-12').removeClass('col-md-12').addClass('col-md-3');
    $('#2columns .col-md-12').removeClass('col-md-12').addClass('col-md-6');
    $('#3columns .col-md-12').removeClass('col-md-12').addClass('col-md-4');
    $('.columnRankContainer .col-md-12').removeClass('col-md-12').addClass('col-md-3');
    $('.columnRankContainer .col-md-6').removeClass('col-md-6').addClass('col-md-3');
    $('.columnRankContainer .col-md-6').removeClass('col-md-6').addClass('col-md-3');
    $('.columnRankContainer').closest('.container').css('width', '100%');
    $('.gridmatrix .title.innerStack').removeClass('innerStack gridmobile gridtablet');
    $('.gridmatrix .matrixOption').removeClass('col-md-12');
    $('.br-wrapper').removeClass('clear');
    $('.ddDesktop').css('display', 'block');
    $('.ddMobtab').addClass('hidden-md hidden-lg');
    $('.typed').removeClass('typedSizetoggle');
    $('.multislider .col-sm-8').removeClass('sliderResponsive');
    $('.multislider .col-sm-4').removeClass('sliderResponsive');
    $('.boxSortCRI .brandBox').addClass('.col-md-columnRankImage').css('width', '25%');
  }
  tablet.onclick = function() {
    $('.container').css('width', '600px');
    $('.multiImageZoom .col-md-3').removeClass('col-md-3').addClass('col-md-6');
    $('.multiImageZoom .col-md-12').removeClass('col-md-12').addClass('col-md-6');
    $('#2columns .col-md-12').removeClass('col-md-12').addClass('col-md-6');
    $('#3columns .col-md-4').removeClass('col-md-4').addClass('col-md-12');
    $('.columnRankContainer .col-md-3').removeClass('col-md-3').addClass('col-md-6');
    $('.columnRankContainer .col-md-12').removeClass('col-md-12').addClass('col-md-6');
    $('.gridmatrix .matrixOption').addClass('col-md-12');
    $('.gridmatrix .title').addClass('innerStack gridtablet');
    $('.br-wrapper').addClass('clear');
    $('.ddDesktop').css('display', 'none');
    $('.ddMobtab').removeClass('hidden-md hidden-lg');
    $('.typed').addClass('typedSizetoggle');
    $('.typedLogo').css('margin-bottom', '0'); 
    $('.multislider .col-sm-8').addClass('sliderResponsive');
    $('.multislider .col-sm-4').addClass('sliderResponsive');
    $('.multislider .sliderResponsive input').css('width', '100%');
    $('.boxSortCRI .brandBox').addClass('.col-md-columnRankImage').css('width', '40%');

  }
  phone.onclick = function() {
    $('.container').css('width', '400px');
    $('.multiImageZoom .col-md-3').removeClass('col-md-3').addClass('col-md-12');
    $('.multiImageZoom .col-md-6').removeClass('col-md-6').addClass('col-md-12');
    $('#2columns .col-md-6').removeClass('col-md-6').addClass('col-md-12');
    $('#3columns .col-md-4').removeClass('col-md-4').addClass('col-md-12');
    $('.columnRankContainer .col-md-6').removeClass('col-md-6').addClass('col-md-12');
    $('.columnRankContainer .col-md-3').removeClass('col-md-3').addClass('col-md-12');
    $('.gridmatrix .title').addClass('innerStack gridmobile');
    $('.gridmatrix .matrixOption').addClass('col-md-12');
    $('.br-wrapper').addClass('clear');
    $('.ddDesktop').css('display', 'none');
    $('.ddMobtab').removeClass('hidden-md hidden-lg');
    $('.typed').addClass('typedSizetoggle');
    $('.typedLogo').css('margin-bottom', '0'); 
    $('.multislider .col-sm-8').addClass('sliderResponsive');
    $('.multislider .col-sm-4').addClass('sliderResponsive');
    $('.multislider .sliderResponsive input').css('width', '100%');
    $('.boxSortCRI .brandBox').addClass('.col-md-columnRankImage').css('width', '55%');
  }
  });
//Typed
$(function(){
	$(".typed").typed({
		strings: ["<span style='color:#EC008C'>TNS</span>", "<span style='color:#BCD435'>Millward Brown</span>", "<span style='color:#A34D98'>Media</span>", "<span style='color:#9D9D9C'>Public</span>"],
		// Optionally use an HTML element to grab strings from (must wrap each string in a <p>)
		stringsElement: null,
		// typing speed
		typeSpeed: 30,
		// time before typing starts
		startDelay: 800,
		// backspacing speed
		backSpeed: 10,
		// time before backspacing
		backDelay: 800,
		// loop
		loop: true,
		// false = infinite
		loopCount: 5,
		// show cursor
		showCursor: false,
		// character for cursor
		cursorChar: "|",
		// attribute to type (null == text)
		attr: null,
		// either html or text
		contentType: 'html',
    fadeOut: true,
    fadeOutDelay: 500,
		// call when done callback function
		callback: function() {},
		// starting callback function before each string
		preStringTyped: function() {},
		//callback for every typed string
		onStringTyped: function() {},
		// callback for reset
		resetCallback: function() {}
	});
});
    function newTyped(){ /* A new typed object */ }
//Navigation
$(function() {
      $( 'li a' ).on( 'click', function() {
            $( 'ul' ).find( 'li a' ).removeClass( 'highlight' );
            $( this ).addClass( 'highlight' );
      });
});
//Select checkmark single question
    $(".list-group a").click(function() {
        $(".list-group a").removeClass('selected-check');
        $(this).addClass("selected-check");
    });

    /*$(".list-group-item").click(function() {
      $(this).find('.col-md-6').each(function() {
        $('.list-group-item').addClass("selected-check");
      });
  });*/

//Select checkmark multi question
  $(".list-group-multi a").click(function() {
    $(this).toggleClass("selected-check");
  });
//Highlight column rank
    $("input[type=checkbox]").on('change', function () {
    $(this).parent().toggleClass("highlight");
    });
//Column rank
$(function(){
    $('.numberOutputCR').hide();
    var count = 0;
    $('.ruleCR').on('change', function () {
        var $currentBadge = $(this).next('.numberOutputCR');
        if (this.checked) {
            count++;
            $currentBadge.show().text(count);
        } else if (!this.checked) {
            count--;
            var thisVal = parseInt($currentBadge.text(),10);
            $currentBadge.hide().text("");
            $(".numberOutputCR:visible").each(function () {
                var val = parseInt($(this).text(),10);
                if (val != 1 && val > thisVal) $(this).text(val- 1);
            });
            var reduce = parseInt($(this).parent().closest('.boxSortCR').find('[type=checkbox]:checked').next('.numberOutputCR').text());
        }
    });
});
//Column rank image
$(function(){
    $('.numberOutputCRI').hide();
    var count = 0;
    $('.ruleCRI').on('change', function () {
        var $currentBadge = $(this).next('.numberOutputCRI');
        if (this.checked) {
            count++;
            $currentBadge.show().text(count);
        } else if (!this.checked) {
            count--;
            var thisVal = parseInt($currentBadge.text(),10);
            $currentBadge.hide().text("");
            $(".numberOutputCRI:visible").each(function () {
                var val = parseInt($(this).text(),10);
                if (val != 1 && val > thisVal) $(this).text(val- 1);
            });
            var reduce = parseInt($(this).parent().closest('.boxSortCRI').find('[type=checkbox]:checked').next('.numberOutputCRI').text());
        }
    });
});
//Autocomplete
$.widget("ui.autocomplete", $.ui.autocomplete, {

  _renderMenu: function(ul, items) {
    var that = this;
    ul.attr("class", "nav nav-pills nav-stacked  bs-autocomplete-menu");
    $.each(items, function(index, item) {
      that._renderItemData(ul, item);
    });
  },

  _resizeMenu: function() {
    var ul = this.menu.element;
    ul.outerWidth(Math.min(
      // Firefox wraps long text (possibly a rounding bug)
      // so we add 1px to avoid the wrapping (#7513)
      ul.width("").outerWidth() + 1,
      this.element.outerWidth()
    ));
  }

});

(function() {
  "use strict";
  var cities = [{
    "id": 1,
    "cityName": "Amsterdam"
  }, {
    "id": 2,
    "cityName": "Athens"
  }, {
    "id": 3,
    "cityName": "Baghdad"
  }, {
    "id": 4,
    "cityName": "Bangkok"
  }, {
    "id": 5,
    "cityName": "Barcelona"
  }, {
    "id": 6,
    "cityName": "Beijing"
  }, {
    "id": 7,
    "cityName": "Belgrade"
  }, {
    "id": 8,
    "cityName": "Berlin"
  }, {
    "id": 9,
    "cityName": "Bogota"
  }, {
    "id": 10,
    "cityName": "Bratislava"
  }, {
    "id": 11,
    "cityName": "Brussels"
  }, {
    "id": 12,
    "cityName": "Bucharest"
  }, {
    "id": 13,
    "cityName": "Budapest"
  }, {
    "id": 14,
    "cityName": "Buenos Aires"
  }, {
    "id": 15,
    "cityName": "Cairo"
  }, {
    "id": 16,
    "cityName": "CapeTown"
  }, {
    "id": 17,
    "cityName": "Caracas"
  }, {
    "id": 18,
    "cityName": "Chicago"
  }, {
    "id": 19,
    "cityName": "Copenhagen"
  }, {
    "id": 20,
    "cityName": "Dhaka"
  }, {
    "id": 21,
    "cityName": "Dubai"
  }, {
    "id": 22,
    "cityName": "Dublin"
  }, {
    "id": 23,
    "cityName": "Frankfurt"
  }, {
    "id": 24,
    "cityName": "Geneva"
  }, {
    "id": 25,
    "cityName": "Hanoi"
  }, {
    "id": 26,
    "cityName": "Helsinki"
  }, {
    "id": 27,
    "cityName": "Hong Kong"
  }, {
    "id": 28,
    "cityName": "Istanbul"
  }, {
    "id": 29,
    "cityName": "Jakarta"
  }, {
    "id": 30,
    "cityName": "Jerusalem"
  }, {
    "id": 31,
    "cityName": "Johannesburg"
  }, {
    "id": 32,
    "cityName": "Kabul"
  }, {
    "id": 33,
    "cityName": "Karachi"
  }, {
    "id": 34,
    "cityName": "Kiev"
  }, {
    "id": 35,
    "cityName": "Kuala Lumpur"
  }, {
    "id": 36,
    "cityName": "Lagos"
  }, {
    "id": 37,
    "cityName": "Lahore"
  }, {
    "id": 38,
    "cityName": "Lima"
  }, {
    "id": 39,
    "cityName": "Lisbon"
  }, {
    "id": 40,
    "cityName": "Ljubljana"
  }, {
    "id": 41,
    "cityName": "London"
  }, {
    "id": 42,
    "cityName": "Los Angeles"
  }, {
    "id": 43,
    "cityName": "Luxembourg"
  }, {
    "id": 44,
    "cityName": "Madrid"
  }, {
    "id": 45,
    "cityName": "Manila"
  }, {
    "id": 46,
    "cityName": "Marrakesh"
  }, {
    "id": 47,
    "cityName": "Melbourne"
  }, {
    "id": 48,
    "cityName": "Mexico City"
  }, {
    "id": 49,
    "cityName": "Montreal"
  }, {
    "id": 50,
    "cityName": "Moscow"
  }, {
    "id": 51,
    "cityName": "Mumbai"
  }, {
    "id": 52,
    "cityName": "Nairobi"
  }, {
    "id": 53,
    "cityName": "New Delhi"
  }, {
    "id": 54,
    "cityName": "NewYork"
  }, {
    "id": 55,
    "cityName": "Nicosia"
  }, {
    "id": 56,
    "cityName": "Oslo"
  }, {
    "id": 57,
    "cityName": "Ottawa"
  }, {
    "id": 58,
    "cityName": "Paris"
  }, {
    "id": 59,
    "cityName": "Prague"
  }, {
    "id": 60,
    "cityName": "Reykjavik"
  }, {
    "id": 61,
    "cityName": "Riga"
  }, {
    "id": 62,
    "cityName": "Rio de Janeiro"
  }, {
    "id": 63,
    "cityName": "Rome"
  }, {
    "id": 64,
    "cityName": "Saint Petersburg"
  }, {
    "id": 65,
    "cityName": "San Francisco"
  }, {
    "id": 66,
    "cityName": "Santiago de Chile"
  }, {
    "id": 67,
    "cityName": "São Paulo"
  }, {
    "id": 68,
    "cityName": "Seoul"
  }, {
    "id": 69,
    "cityName": "Shanghai"
  }, {
    "id": 70,
    "cityName": "Singapore"
  }, {
    "id": 71,
    "cityName": "Sofia"
  }, {
    "id": 72,
    "cityName": "Stockholm"
  }, {
    "id": 73,
    "cityName": "Sydney"
  }, {
    "id": 74,
    "cityName": "Tallinn"
  }, {
    "id": 75,
    "cityName": "Tehran"
  }, {
    "id": 76,
    "cityName": "The Hague"
  }, {
    "id": 77,
    "cityName": "Tokyo"
  }, {
    "id": 78,
    "cityName": "Toronto"
  }, {
    "id": 79,
    "cityName": "Venice"
  }, {
    "id": 80,
    "cityName": "Vienna"
  }, {
    "id": 81,
    "cityName": "Vilnius"
  }, {
    "id": 82,
    "cityName": "Warsaw"
  }, {
    "id": 83,
    "cityName": "Washington D.C."
  }, {
    "id": 84,
    "cityName": "Wellington"
  }, {
    "id": 85,
    "cityName": "Zagreb"
  }];

  $('.bs-autocomplete').each(function() {
    var _this = $(this),
      _data = _this.data(),
      _hidden_field = $('#' + _data.hidden_field_id);

    _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
      .parent('.form-group').addClass('has-feedback');

    var feedback_icon = _this.next('.bs-autocomplete-feedback');
    feedback_icon.hide();

    _this.autocomplete({
        minLength: 2,
        autoFocus: true,

        source: function(request, response) {
          var _regexp = new RegExp(request.term, 'i');
          var data = cities.filter(function(item) {
            return item.cityName.match(_regexp);
          });
          response(data);
        },

        search: function() {
          feedback_icon.show();
          _hidden_field.val('');
        },

        response: function() {
          feedback_icon.hide();
        },

        focus: function(event, ui) {
          _this.val(ui.item[_data.item_label]);
          event.preventDefault();
        },

        select: function(event, ui) {
          _this.val(ui.item[_data.item_label]);
          _hidden_field.val(ui.item[_data.item_id]);
          event.preventDefault();
        }
      })
      .data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li></li>')
          .data("item.autocomplete", item)
          .append('<a>' + item[_data.item_label] + '</a>')
          .appendTo(ul);
      };
    // end autocomplete
  });
})();

//Image modal
$(function() {
    var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).closest('.bizcontent').find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});

//Number slider
  $("#slider1").slider( {
      range: "min",
      value: 1,
      step: 1,
      min: 0,
      max: 100,
      slide: function( event, ui ) {
          $("input.singleInput").val(ui.value);
      }
  });
  $("#slider2").slider( {
      range: "min",
      value: 1,
      step: 1,
      min: 0,
      max: 100,
      slide: function( event, ui ) {
             update();
      }
  });
  $("#slider3").slider( {
    range: "min",
    value: 1,
    step: 1,
    min: 0,
    max: 100,
    slide: function( event, ui ) {
           update();
    }
  });
  $("#slider4").slider( {
    range: "min",
    value: 1,
    step: 1,
    min: 0,
    max: 100,
    slide: function( event, ui ) {
           update();
    }
  });

function update() {
    $amount1 = $("#slider2").slider('values', 0);
    $amount2 = $("#slider3").slider('values', 0);
    $amount3 = $("#slider4").slider('values', 0);
    $amount4 = $amount1 + $amount2 + $amount3;
    $('.multipleInput').val($amount1);
    $('.multipleInput1').val($amount2);
    $('.multipleInput2').val($amount3);
    $('#sliderResult').val($amount4);
  };
/*Multiple Number Input*/
$(function() {
  $('.sliderInputMulti input').keyup(function() {
    var val1 = +$('.multipleNumberInput1').val();
    var val2 = +$('.multipleNumberInput2').val();
    var val3 = +$('.multipleNumberInput3').val();
    var val4 = +$('.multipleNumberInput4').val();
    var val5 = +$('.multipleNumberInput5').val();
    var val6 = +$('.multipleNumberInput6').val();
    var val7 = +$('.multipleNumberInput7').val();
    var val8 = +$('.multipleNumberInput8').val();
    var val9 = +$('.multipleNumberInput9').val();
    var val10 = +$('.multipleNumberInput10').val();
    $('#totalNumberInput').val(val1+val2+val3+val4+val5+val6+val7+val8+val9+val10);
  });
});
//Calendar
$(function() {
  $('.date').datepicker({
    format: 'dd-mm-yyyy'
  });
});

//Grid Matrix
$(".matrixOption").click(function(e) {
  var prev = $(this).parent().next().find('.matrixOption');
  if($(prev).length > 0) {
  $('html,body').animate({ scrollTop:$(this).parent().next().offset().top}, 'slow');
  }
  e.preventDefault();
});

$("a.matrixSelection").click(function() {
  $(this).parent(".matrixOption").siblings().removeClass("active");
  $(this).parent().addClass("active");
  $(this).parent(".matrixOption").siblings().addClass("is-disabled");
});
$(".matrixOption").click(function() {
  $(this).closest('.innerStack').hide();
});

//Radio Slider
var pricelevel = ["Very Low", "Low", "Average", "High", "Very high"];        
$(".sliderPip")
  .slider({
    min: 0,
    max: 4,
    range: "min",
    animate: "fast"
  })
  .slider("pips", {
    rest: "label",
    labels: pricelevel
  })

//Star rating
$(function() {
      $('.starRating').barrating({
        theme: 'fontawesome-stars',
        deselectable: false
      });
      $('.br-widget a:nth-of-type(1)').tooltip({ 'title': 'Totally unacceptable' });
      $('.br-widget a:nth-of-type(2)').tooltip({ 'title': 'Unacceptable' });
      $('.br-widget a:nth-of-type(3)').tooltip({ 'title': 'Neutral' });
      $('.br-widget a:nth-of-type(4)').tooltip({ 'title': 'Acceptable' });
      $('.br-widget a:nth-of-type(5)').tooltip({ 'title': 'Perfectly acceptable' });

      $(".br-widget a").click(function() {
          $(this).closest(".starRatingContainer").find(".starRatingCheck").addClass("selected-check left");
      });  
});

//Swipe Cards
$(document).ready(function(event) {
	
	$("div#swipe_like").on( "click", function() {
		swipeLike();
	});	

	$("div#swipe_dislike").on( "click", function() {
		swipeDislike();
	});	

	addNewProfile();

	function swipe() {
		Draggable.create("#photo", {
		   	throwProps:true,
		   	onDragEnd:function(endX) {
	   			if(Math.round(this.endX) > 0 ) {
	   				swipeLike();			
	   			}
	   			else {
	   				swipeDislike();
	   			}
	   			console.log(Math.round(this.endX));
			}
		});
	}

	function swipeLike() {
		
			var $photo = $("div.content").find('#photo');

			var swipe = new TimelineMax({repeat:0, yoyo:false, repeatDelay:0, onComplete:remove, onCompleteParams:[$photo]});
			swipe.staggerTo($photo, 0.8, {bezier:[{left:"+=400", top:"+=300", rotation:"60"}], ease:Power1.easeInOut});

			addNewProfile();
	}

	function swipeDislike() {
		
			var $photo = $("div.content").find('#photo');

			var swipe = new TimelineMax({repeat:0, yoyo:false, repeatDelay:0, onComplete:remove, onCompleteParams:[$photo]});
			swipe.staggerTo($photo, 0.8, {bezier:[{left:"+=-350", top:"+=300", rotation:"-60"}], ease:Power1.easeInOut});

			addNewProfile();
	}

	function remove(photo) {
	    $(photo).remove();
	}

	function addNewProfile() {
		var photos = ['horse', 'pinguin', 'cat', 'dog', 'shark', 'giraffe', 'sloth', 'tiger' ][Math.floor(Math.random() *8)]
		$("div.content").prepend('<div class="photo" id="photo" style="background-image:url(images/'+photos+'.jpg)">'
    	+ '<span class="meta">' 
    	+ '<p>'+photos+'</p>' 
    	+ '</span>' 
    	+ '</div>');

    	swipe();
  }
});
/*Swipe Cards overflow hidden
$(function() {
  $(document).click(function() {
  if ($('.swipehigh').hasClass('highlight')) {
      $('body').addClass('overflowhidden');
  }
  else {
    $('body').removeClass('overflowhidden');
  }
});
});
*/