<?php
// including the database connection file
include_once("config.php");

if(isset($_POST['update']))
{	

	$id = mysqli_real_escape_string($mysqli, $_POST['id']);
	
	$dateProject = mysqli_real_escape_string($mysqli, $_POST['dateProject']);
	$fieldworkDate = mysqli_real_escape_string($mysqli, $_POST['fieldworkDate']);
	$namePerson = mysqli_real_escape_string($mysqli, $_POST['namePerson']);	
	$projectId = mysqli_real_escape_string($mysqli, $_POST['projectId']);	
	$nameClient = mysqli_real_escape_string($mysqli, $_POST['nameClient']);	
	$surveyMet = mysqli_real_escape_string($mysqli, ImplodeArrayToString($_POST['surveyMet']));	
	$sampleSource = mysqli_real_escape_string($mysqli, ImplodeArrayToString($_POST['sampleSource']));
	$sampleReceived = mysqli_real_escape_string($mysqli, ImplodeArrayToString($_POST['sampleReceived']));
	$applicationsUsed = mysqli_real_escape_string($mysqli, $_POST['applicationsUsed']);
	$collectedFrom = mysqli_real_escape_string($mysqli, ImplodeArrayToString($_POST['collectedFrom']));
	$collectedType = mysqli_real_escape_string($mysqli, ImplodeArrayToString($_POST['collectedType']));
	$pseudo = mysqli_real_escape_string($mysqli, $_POST['pseudo']);
	$privacyPolicy = mysqli_real_escape_string($mysqli, $_POST['privacyPolicy']);
	$consent = mysqli_real_escape_string($mysqli, $_POST['consent']);
	$thirdYesNo = mysqli_real_escape_string($mysqli, $_POST['thirdYesNo']);
	$thirdParty = mysqli_real_escape_string($mysqli, $_POST['thirdParty']);
	$purposeSharing = mysqli_real_escape_string($mysqli, $_POST['purposeSharing']);
	$shareData = mysqli_real_escape_string($mysqli, $_POST['shareData']);
		{
		//updating the table
		$result = mysqli_query($mysqli, "UPDATE users SET dateProject='$dateProject',fieldworkDate='$fieldworkDate',namePerson='$namePerson',projectId='$projectId',nameClient='$nameClient',surveyMet='$surveyMet',sampleSource='$sampleSource',sampleReceived='$sampleReceived',applicationsUsed='$applicationsUsed',collectedFrom='$collectedFrom',collectedType='$collectedType',pseudo='$pseudo',privacyPolicy='$privacyPolicy',consent='$consent',thirdYesNo='$thirdYesNo',thirdParty='$thirdParty',purposeSharing='$purposeSharing',shareData='$shareData',test='$test' WHERE id=$id");
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}

function ImplodeArrayToString($input)
{
	$implodedVar = implode(", ", $input);
	return $implodedVar;
}
?>
<?php
//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = mysqli_query($mysqli, "SELECT * FROM users WHERE id=$id");

while($res = mysqli_fetch_array($result))
{	
	$dateProject = $res['dateProject'];
	$fieldworkDate = $res['fieldworkDate'];
	$namePerson = $res['namePerson'];
	$projectId = $res['projectId'];
	$nameClient = $res['nameClient'];
	$surveyMet = $res['surveyMet'];
	$sampleSource = $res['sampleSource'];
	$sampleReceived = $res['sampleReceived'];
	$applicationsUsed = $res['applicationsUsed'];
	$collectedFrom = $res['collectedFrom'];
	$collectedType = $res['collectedType'];
	$pseudo = $res['pseudo'];
	$privacyPolicy = $res['privacyPolicy'];
	$consent = $res['consent'];
	$thirdYesNo = $res['thirdYesNo'];
	$thirdParty = $res['thirdParty'];
	$purposeSharing = $res['purposeSharing'];
	$shareData = $res['shareData'];
	$test = $res['test'];
}
?>
<html>
<head>	
	<title>Edit Data</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

	<script>
		$(document).ready(function(){
		$('.openText').change(function(){
			var id = $(this).find('option:selected').attr('id');

				switch (id){
					case "surveymet":
						$('.dropdownTextMet').css({'display':'block','visibility':'initial'});
						$('.dropdownTextMet input').attr('name', 'surveyMet[]');;   
						break;
					case "sample_source":
						$('.dropdownTextSource').css({'display':'block','visibility':'initial'});
						$('.dropdownTextSource input').attr('name', 'sampleSource[]');;   
						break;
					case "sample_received":
						$('.dropdownTextRecei').css({'display':'block','visibility':'initial'});
						$('.dropdownTextRecei input').attr('name', 'sampleReceived[]');; 
						break;  
					case "data_collect":
						$('.dropdownTextCollect').css({'display':'block','visibility':'initial'});
						$('.dropdownTextCollect input').attr('name', 'collectedFrom[]');; 
						break;
					case "collect_type":
						$('.dropdownTextType').css({'display':'block','visibility':'initial'});
						$('.dropdownTextType input').attr('name', 'collectedType[]');; 
						break;    
					case "yesNoCollapse":
						$('#thirdYesNoCollapse').css({'display':'block','visibility':'initial'});
						break;
					case "goneTextMet":
						$('.dropdownTextMet').css({'display':'none','visibility':'hidden'});
						break;
					case "goneTextSource":
						$('.dropdownTextSource').css({'display':'none','visibility':'hidden'});
						break;
					case "goneTextReceive":
						$('.dropdownTextRecei').css({'display':'none','visibility':'hidden'});
						break;
					case "goneTextCollect":
						$('.dropdownTextCollect').css({'display':'none','visibility':'hidden'});
						break;
					case "goneCollectType":
						$('.dropdownTextType').css({'display':'none','visibility':'hidden'});
						break;
					case "goneYesNoCollapse":
						$('#thirdYesNoCollapse').css({'display':'none','visibility':'hidden'});
				}
				});

/* 			if ($(this).val() == '1' || $(this).val() == '2') {
				$('#dropdownText').css({'display':'block','visibility':'initial'});
				$('#dropdownText input').attr('name', 'sampleSource');;           
			}
			else {
				$('#dropdownText').css({'visibility':'hidden'});
			}
			}); */
		});
		$(document).ready(function(){
		$('.openText').change(function(){
			if ($(this).find('option:selected').is('#surveymet') && $(this).find('option:selected').is('#goneTextMet')) {
				$('.dropdownTextMet').css({'display':'block','visibility':'initial'});
				$('.dropdownTextMet input').attr('name', 'surveyMet[]');
			} else if ($(this).find('option:selected').is('#sample_source') && $(this).find('option:selected').is('#goneTextSource')) {
				$('.dropdownTextSource').css({'display':'block','visibility':'initial'})
				$('.dropdownTextSource input').attr('name', 'sampleSource[]');   
			} else if ($(this).find('option:selected').is('#sample_received') && $(this).find('option:selected').is('#goneTextReceive')) {
				$('.dropdownTextRecei').css({'display':'block','visibility':'initial'})
				$('.dropdownTextRecei input').attr('name', 'sampleReceived[]');   
			} else if ($(this).find('option:selected').is('#data_collect') && $(this).find('option:selected').is('#goneTextCollect')) {
				$('.dropdownTextCollect').css({'display':'block','visibility':'initial'})
				$('.dropdownTextCollect input').attr('name', 'collectedFrom[]'); 
			} else if ($(this).find('option:selected').is('#collect_type') && $(this).find('option:selected').is('#goneCollectType')) {
				$('.dropdownTextType').css({'display':'block','visibility':'initial'})
				$('.dropdownTextType input').attr('name', 'collectedType[]');
			}
		});
	});

		$(function(){
			if($('#thirdYN option:selected').val() == 'Yes'){
				$('#thirdYesNoCollapse').css({'display':'block','visibility':'initial'});
			}
		});

		$(document).ready(function(){
			$('.openText').change(function(){
			if($('#thirdYN option:selected').val() == 'No'){
				$('input#thirdParty').attr('value', '');;  
				$('textarea#purposeshare').attr('value', '').empty();; 
				$('#shareda option:selected').attr('value', '').empty();; 
			}
		});
		});

	</script>

</head>

<body>
<div class="container-fluid content">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">
          <img src="kantarlogo.png" width="120" alt="Kantar GDPR">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
</nav>
	<form name="form1" method="post" action="edit.php">
	<table class="tableEdit" border="0">
			<tr> 
				<td>Date of project inventory completion *</td>
				<td><input type="date" name="dateProject" required value="<?php echo $dateProject;?>"></td>
			</tr>
			<tr> 
				<td>Fieldwork date</td>
				<td><input type="date" name="fieldworkDate" value="<?php echo $fieldworkDate;?>"></td>
			</tr>
			<tr> 
				<td>Name of person completing the checklist*</td>
				<td><input type="text" name="namePerson" required value="<?php echo $namePerson;?>"></td>
			</tr>
			<tr> 
				<td>Project ID/reference (Maconomy job number)*</td>
				<td><input type="text" name="projectId" required value="<?php echo $projectId;?>"></td>
			</tr>
			<tr> 
				<td>Name of client</td>
				<td><input type="text" name="nameClient" value="<?php echo $nameClient;?>"></td>
			</tr>
			<tr> 
				<td>Survey Methodology</td>
				<td>
					<select class="openText" id="sm" name="surveyMet[]" multiple value="<?php echo $surveyMet;?>">
						<option value="<?php echo $sampleSource;?>"selected hidden><?php echo $sampleSource;?></option>
						<option id="goneTextMet" value="Ad Exposure - (Cookie tracking)">Ad Exposure - (Cookie tracking)</option>
						<option id="goneTextMet" value="Ad Exposure - Audio Content Recognition">Ad Exposure - Audio Content Recognition</option>
						<option id="goneTextMet" value="Ad Exposure - TV meter">Ad Exposure - TV meter</option>
						<option id="goneTextMet" value="Connection -  Data Matches - Matching panellists by addresses using non–networked computers">Connection -  Data Matches - Matching panellists by addresses using non-networked computers</option>
						<option id="goneTextMet" value="Connection - Panel Data Enrichment">Connection - Panel Data Enrichment</option>
						<option id="goneTextMet" value="Connection - Panel Data Enrichment by DMP (Research Insights)">Connection - Panel Data Enrichment by DMP (Research Insights)</option>
						<option id="goneTextMet" value="Diary - offline">Diary - offline</option>
						<option id="goneTextMet" value="Diary - online (including communities/platforms with video/photo assignments for respondents (3rd parties could be filmed/photographed)">Diary - online (including communities/platforms with video/photo assignments for respondents (3rd parties could be filmed/photographed)</option>
						<option id="goneTextMet" value="Inference - Look-A-Like Audience Activation (w Seed Removal)">Inference - Look-A-Like Audience Activation (w Seed Removal)</option>
						<option id="goneTextMet" value="Inference - Segmentation of respondents (using Kantar products)">Inference - Segmentation of respondents (using Kantar products)</option>
						<option id="goneTextMet" value="Observational - Ethnography - both full ethnography studies and pre-work captured for qual studies">Observational - Ethnography - both full ethnography studies and pre-work captured for qual studies</option>
						<option id="goneTextMet" value="Observational - facial recognition data">Observational - facial recognition data</option>
						<option id="goneTextMet" value="Observational - In store path tracking with video cameras or inbeacon technology">Observational - In store path tracking with video cameras or inbeacon technology</option>
						<option id="goneTextMet" value="Observational - Online qualitative - discussions, ratings, co-creation of ideas">Observational - Online qualitative - discussions, ratings, co-creation of ideas</option>
						<option id="goneTextMet" value="Observational - FTF qualitative - discussions, ratings, co-creation of ideas">Observational - FTF qualitative - discussions, ratings, co-creation of ideas</option>
						<option id="goneTextMet" value="Observational - Telephone qualitative - discussions, ratings, co-creation of ideas">Observational - Telephone qualitative - discussions, ratings, co-creation of ideas</option>
						<option id="goneTextMet" value="Observational - other biometric (e.g. eye-tracking)">Observational - other biometric (e.g. eye-tracking)</option>
						<option id="goneTextMet" value="Observational - Client observation">Observational - Client observation</option>
						<option id="goneTextMet" value="Observational - qualitative transcripts">Observational - qualitative transcripts</option>
						<option id="goneTextMet" value="Observational - qualitative videos or observation (e.g. camera recording in a living space)">Observational - qualitative videos or observation (e.g. camera recording in a living space)</option>
						<option id="goneTextMet" value="Observational - Social Media monitoring - (e.g. Facebook, Twitter etc.)">Observational - Social Media monitoring - (e.g. Facebook, Twitter etc.)</option>
						<option id="goneTextMet" value="Observational - wearable or other tracking device (e.g. Fitbit, Apple watch, or geolocator device etc.)">Observational - wearable or other tracking device (e.g. Fitbit, Apple watch, or geolocator device etc.)</option>
						<option id="goneTextMet" value="Passive Monitoring - 3rd party meter">Passive Monitoring - 3rd party meter</option>
						<option id="goneTextMet" value="Passive Monitoring - email and phone activity">Passive Monitoring - email and phone activity</option>
						<option id="goneTextMet" value="Passive Monitoring - Kantar owned meter">Passive Monitoring - Kantar owned meter</option>
						<option id="goneTextMet" value="Passive Monitoring - location">Passive Monitoring - location</option>
						<option id="goneTextMet" value="Passive Monitoring - phone/network usage information">Passive Monitoring - phone/network usage information</option>
						<option id="goneTextMet" value="Passive Monitoring - telemetry - keystroke tracking or other">Passive Monitoring - telemetry - keystroke tracking or other</option>
						<option id="goneTextMet" value="Passive Monitoring - websites & apps visited">Passive Monitoring - websites & apps visited</option>
						<option id="goneTextMet" value="Purchase tracking - bar code/survey capture">Purchase tracking - bar code/survey capture</option>
						<option id="goneTextMet" value="Purchase Tracking - invoice capture (online and/or via photo)">Purchase Tracking - invoice capture (online and/or via photo)</option>
						<option id="goneTextMet" value="Purchase Tracking - loyalty card data">Purchase Tracking - loyalty card data</option>
						<option id="goneTextMet" value="Purchase Tracking - respondent supplied product photos">Purchase Tracking - respondent supplied product photos</option>
						<option id="goneTextMet" value="Survey - chatbot or other AI">Survey - chatbot or other AI</option>
						<option id="goneTextMet" value="Survey - offline, FTF (CAPI or PAPI)">Survey - offline, FTF (CAPI or PAPI)</option>
						<option id="goneTextMet" value="Survey - offline, SMS/text">Survey - offline, SMS/text</option>
						<option id="goneTextMet" value="Survey - offline, telephone">Survey - offline, telephone</option>
						<option id="goneTextMet" value="Survey - online + offline blend">Survey - online + offline blend</option>
						<option id="goneTextMet" value="Survey - online, client sample">Survey - online, client sample</option>
						<option id="goneTextMet" value="Survey - online, 3rd party panel">Survey - online, 3rd party panel</option>
						<option id="goneTextMet" value="Survey - online, 3rd party sample">Survey - online, 3rd party sample</option>
						<option id="goneTextMet" value="Survey - online, blended panels">Survey - online, blended panels</option>
						<option id="goneTextMet" value="Survey - online, Kantar owned panel">Survey - online, Kantar owned panel</option>
						<option id="goneTextMet" value="Survey - purchased from 3rd party">Survey - purchased from 3rd party</option>
						<option id="goneTextMet" value="3rd party data provision - respondent supplied">3rd party data provision - respondent supplied</option>
						<option id="surveymet" label="selected" value="Other methodology - specify:">Other methodology - specify:</option>
					</select>
					<div class="dropdownTextMet" style="display: none">Specify: <input type="text"/>	
				</td>
			</tr>
			<tr> 
				<td>What is the sample source?</td>
				<td>
					<select class="openText" name="sampleSource[]" multiple value="<?php echo $sampleSource;?>">
					<option value="<?php echo $sampleSource;?>"selected hidden><?php echo $sampleSource;?></option>
					<option id="goneTextSource" value="Client supplied sample">Client supplied sample</option>
					<option id="sample_source" label="selected" value="Public domain list sample - specify below:">Public domain list sample - specify below:</option>
					<option id="sample_source" label="selected" value="Random sample - specify below:">Random sample - specify below:</option>
					<option id="sample_source" label="selected" value="Non-client supplied named sample (e.g. via a 3rd party) - specify below:">Non-client supplied named sample (e.g. via a 3rd party) - specify below:</option>
					<option id="sample_source" label="selected" value="Kantar owned panel - specify below:">Kantar owned panel - specify below:</option>
					<option id="sample_source" label="selected" value="3rd party owned panel - specify below:">3rd party owned panel - specify below:</option>
					<option id="sample_source" label="selected" value="Kantar owned and 3rd party owned panel - specify below:">Kantar owned and 3rd party owned panel - specify below:</option>
					<option id="sample_source" label="selected" value="Other sample - specify below:">Other sample - specify below:</option>
					</select>
					<div class="dropdownTextSource" style="display: none">Specify: <input type="text"/>
				</div>
				</td>
			</tr>
			<tr> 
				<td>How is the sample received?</td>
				<td>
					<select class="openText" name="sampleReceived[]" multiple value="<?php echo $sampleReceived;?>">
					<option value="<?php echo $sampleReceived;?>"selected hidden><?php echo $sampleReceived;?></option>
					<option id="goneTextReceive" value="Client supplied sample">Client supplied sample</option>
					<option id="goneTextReceive" value="Ad Exposure - (Cookie tracking)">Ad Exposure - (Cookie tracking)</option>
					<option id="sample_received" label="selected" value="Other sample - specify below:">Other sample - specify below:</option>
					</select>
					<div class="dropdownTextRecei" style="display: none">Specify: <input type="text"/>
				</div>
				</td>
			</tr>
			<tr> 
				<td>List the applications used in the project<br/>(e.g. Nfield, Unicom Intelligence, Decipher)</td>
				<td><input type="text" name="applicationsUsed" value="<?php echo $applicationsUsed;?>"></td>
			</tr>
			<tr> 
			<td>Where is the Personal Data being collected from?</td>
				<td>
					<select class="openText" name="collectedFrom[]" multiple value="<?php echo $collectedFrom;?>">
					<option value="<?php echo $collectedFrom;?>"selected hidden><?php echo $collectedFrom;?></option>
					<option id="goneTextCollect" value="Client supplied sample">Client supplied sample</option>
					<option id="goneTextCollect" value="Austria">Austria</option>
					<option id="goneTextCollect" value="Belgium">Belgium</option>
					<option id="goneTextCollect" value="Bulgaria">Bulgaria</option>
					<option id="goneTextCollect" value="Croatia">Croatia</option>
					<option id="goneTextCollect" value="Republic of Cyprus">Republic of Cyprus</option>
					<option id="goneTextCollect" value="Czech Republic">Czech Republic</option>
					<option id="goneTextCollect" value="Denmark">Denmark</option>
					<option id="goneTextCollect" value="Estonia">Estonia</option>
					<option id="goneTextCollect" value="Finland">Finland</option>
					<option id="goneTextCollect" value="France">France</option>
					<option id="goneTextCollect" value="Germany">Germany</option>
					<option id="goneTextCollect" value="Greece">Greece</option>
					<option id="goneTextCollect" value="Hungary">Hungary</option>
					<option id="goneTextCollect" value="Iceland">Iceland</option>
					<option id="goneTextCollect" value="Ireland">Ireland</option>
					<option id="goneTextCollect" value="Italy">Italy</option>
					<option id="goneTextCollect" value="Latvia">Latvia</option>
					<option id="goneTextCollect" value="Liechtenstein">Liechtenstein</option>
					<option id="goneTextCollect" value="Lithuania">Lithuania</option>
					<option id="goneTextCollect" value="Luxembourg">Luxembourg</option>
					<option id="goneTextCollect" value="Malta">Malta</option>
					<option id="goneTextCollect" value="Netherlands">Netherlands</option>
					<option id="goneTextCollect" value="Norway">Norway</option>
					<option id="goneTextCollect" value="Poland">Poland</option>
					<option id="goneTextCollect" value="Portugal">Portugal</option>
					<option id="goneTextCollect" value="Romania">Romania</option>
					<option id="goneTextCollect" value="Slovakia">Slovakia</option>
					<option id="goneTextCollect" value="Slovenia">Slovenia</option>
					<option id="goneTextCollect" value="Spain">Spain</option>
					<option id="goneTextCollect" value="Sweden">Sweden</option>
					<option id="goneTextCollect" value="UK">UK</option>
					<option id="data_collect" label="selected" value="Other - specify below:">Other - specify below:</option>
					</select>
					<div class="dropdownTextCollect" style="display: none">Specify: <input type="text"/>
				</div>
				</td>
			</tr>
			<tr> 
			<td>List the types of Personal Data collected/processed</td>
				<td>
					<select class="openText" name="collectedType[]" multiple value="<?php echo $collectedType;?>">
					<option value="<?php echo $collectedType;?>"selected hidden><?php echo $collectedType;?></option>
					<option id="collect_type" label="selected" value="Personal Data (see definition GDPR Konnect site) - specify:">Personal Data (see definition GDPR Konnect site) - specify:</option>
					<option id="collect_type" label="selected" value="Combination of background data which could identify an indivdual - specify:">Combination of background data which could identify an indivdual - specify:</option>
					<option id="goneCollectType" value="Open ends (which could hold personal data)">Open ends (which could hold personal data)</option>
					<option id="goneCollectType" value="Recordings/Images">Recordings/Images</option>
					<option id="goneCollectType" value="Psuedonymized ID (e.g. Respondent ID)">Psuedonymized ID (e.g. Respondent ID)</option>
					<option id="goneCollectType" value="Sensitive data - Racial or ethnic origin">Sensitive data - Racial or ethnic origin</option>
					<option id="goneCollectType" value="Sensitive data - Political opinions">Sensitive data - Political opinions</option>
					<option id="goneCollectType" value="Sensitive data - Religious or similar beliefs">Sensitive data - Religious or similar beliefs</option>
					<option id="goneCollectType" value="Sensitive data - Trade union membership (or non-membership)">Sensitive data - Trade union membership (or non-membership)</option>
					<option id="goneCollectType" value="Sensitive data - Physical or mental health or condition">Sensitive data - Physical or mental health or condition</option>
					<option id="goneCollectType" value="Sensitive data - Criminal offences, or related proceedings">Sensitive data - Criminal offences, or related proceedings</option>
					<option id="goneCollectType" value="Children's data (respondent younger than 16)">Children's data (respondent younger than 16)</option>
					<option id="goneCollectType" value="None">None</option>
					</select>
					<div class="dropdownTextType" style="display: none">Specify: <input type="text"/></div>
				</td>
			</tr>
			<tr> 
					<td>Is the data Pseudonymized or Anonymized?</td>
					<td>
						<select class="openText" name="pseudo" value="<?php echo $pseudo;?>">
						<option value="<?php echo $pseudo;?>" selected hidden><?php echo $pseudo;?></option>
						<option value="Pseudonymized">Pseudonymized</option>
						<option value="Anonymized">Anonymized</option>
						<option value="None">None</option>
						</select>
						</td>
				</tr>
			<tr> 
					<td>Privacy policy - provide link or location</td>
					<td><input type="text" name="privacyPolicy" value="<?php echo $privacyPolicy;?>"></td>
			</tr>
			<tr>
					<td>Is consent being used by Kantar? If so, detail how consent to collect/process Personal Data will be collected/processed from the respondent. <br/> If not, provide justification</td>
					<td><textarea name="consent" maxlength="250" style="width:100%; padding:5px 10px" value="<?php echo $consent;?>"><?php echo $consent;?></textarea></td>
					<td><i>Max 250 characters</i></td>
			</tr>
			<tr> 
					<td>Parties associated?</td>
					<td>
						<select class="openText" id="thirdYN" name="thirdYesNo">
							<option value="<?php echo $thirdYesNo;?>"selected hidden><?php echo $thirdYesNo;?></option>
							<option id="yesNoCollapse" value="Yes">Yes</option>
							<option id="goneYesNoCollapse" value="No">No</option>
						</select>
						<div id="thirdYesNoCollapse" style="display: none">
							<label>Name of third party</label>
							<input type="text" name="thirdParty" id="thirdParty" value="<?php echo $thirdParty?>">
							
							<label>Purpose for sharing?</label>
							<textarea id="purposeshare" name="purposeSharing" maxlength="250" style="width:100%; padding:5px 10px" value="<?php echo $purposeSharing;?>"><?php echo $purposeSharing;?></textarea>
							<i>Max 250 characters</i>

							<label style="display:block; margin-top:5px">Will they share personal data with other third party?</label>
							<select class="openText" id="shareda" name="shareData">
								<option value="<?php echo $shareData;?>"selected hidden><?php echo $shareData;?></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>	
					</td>		

			</tr>	
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
	</div>

  



</body>
</html>
