<?php
// including the database connection file
include_once("config.php");

if(isset($_POST['update']))
{	

	$id = mysqli_real_escape_string($mysqli, $_POST['id']);
	$dateProject = mysqli_real_escape_string($mysqli, $_POST['dateProject']);
	$fieldworkDate = mysqli_real_escape_string($mysqli, $_POST['fieldworkDate']);
	$namePerson = mysqli_real_escape_string($mysqli, $_POST['namePerson']);
	$projectId = mysqli_real_escape_string($mysqli, $_POST['projectId']);
	$nameClient = mysqli_real_escape_string($mysqli, $_POST['nameClient']);
	$surveyMet = mysqli_real_escape_string($mysqli, $_POST['surveyMet']);		
	
	// checking empty fields
	if(empty($dateProject) || empty($namePerson) || empty($projectId)) {	
			
		if(empty($dateProject)) {
			echo "<font color='red'>Date of project field is empty.</font><br/>";
		}
		
		if(empty($namePerson)) {
			echo "<font color='red'>Name of person field is empty.</font><br/>";
		}
		
		if(empty($projectId)) {
			echo "<font color='red'>Project ID/reference field is empty.</font><br/>";
		}		
	} else {	
		//updating the table
		$result = mysqli_query($mysqli, "UPDATE users SET dateProject='$dateProject',fieldworkDate'$fieldworkDate',namePerson='$namePerson',projectId='$projectId',nameClient='$nameClient',surveyMet='$surveyMet' WHERE id=$id");
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}
?>
<?php
//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = mysqli_query($mysqli, "SELECT * FROM users WHERE id=$id");

while($res = mysqli_fetch_array($result))
{
	$dateProject = $res['dateProject'];
	$fieldworkDate = $res['fieldworkDate'];
	$namePerson = $res['namePerson'];
	$projectId = $res['projectId'];
	$nameClient = $res['nameClient'];
	$surveyMet = $res['surveyMet'];
}
?>
<html>
<head>	
	<title>Edit Data</title>
</head>

<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="form1" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Date of project inventory completion *</td>
				<td><input type="date" name="dateProject" value="<?php echo $dateProject;?>"></td>
			</tr>
			<tr> 
				<td>Fieldwork date</td>
				<td><input type="date" name="fieldworkDate" value="<?php echo $fieldworkDate;?>"></td>
			</tr>
			<tr> 
				<td>Name of person completing the checklist*</td>
				<td><input type="text" name="namePerson" required value="<?php echo $namePerson;?>"></td>
			</tr>
			<tr> 
				<td>Project ID/reference (Maconomy job number)*</td>
				<td><input type="text" name="projectId" required value="<?php echo $projectId;?>"></td>
			</tr>
			<tr> 
				<td>Name of client</td>
				<td><input type="text" name="nameClient" value="<?php echo $nameClient;?>"></td>
			</tr>
			<tr> 
				<td>Survey Methodology</td>
				<td>
					<select name="surveyMet" value="<?php echo $surveyMet;?>">
						<option value="<?php echo $surveyMet;?>" selected disabled hidden><?php echo $surveyMet;?></option>
						<option value="Ad Exposure - (Cookie tracking)">Ad Exposure - (Cookie tracking)</option>
						<option value="Ad Exposure - Audio Content Recognition">Ad Exposure - Audio Content Recognition</option>
						<option value="Ad Exposure - TV meter">Ad Exposure - TV meter</option>
						<option value="Connection -  Data Matches - Matching panellists by addresses using non–networked computers">Connection -  Data Matches - Matching panellists by addresses using non-networked computers</option>
						<option value="Connection - Panel Data Enrichment">Connection - Panel Data Enrichment</option>
						<option value="Connection - Panel Data Enrichment by DMP (Research Insights)">Connection - Panel Data Enrichment by DMP (Research Insights)</option>
						<option value="Diary - offline">Diary - offline</option>
						<option value="Diary - online (including communities/platforms with video/photo assignments for respondents (3rd parties could be filmed/photographed)">Diary - online (including communities/platforms with video/photo assignments for respondents (3rd parties could be filmed/photographed)</option>
						<option value="Inference - Look-A-Like Audience Activation (w Seed Removal)">Inference - Look-A-Like Audience Activation (w Seed Removal)</option>
						<option value="Inference - Segmentation of respondents (using Kantar products)">Inference - Segmentation of respondents (using Kantar products)</option>
						<option value="Observational - Ethnography - both full ethnography studies and pre-work captured for qual studies">Observational - Ethnography - both full ethnography studies and pre-work captured for qual studies</option>
						<option value="Observational - facial recognition data">Observational - facial recognition data</option>
						<option value="Observational - In store path tracking with video cameras or inbeacon technology">Observational - In store path tracking with video cameras or inbeacon technology</option>
						<option value="Observational - Online qualitative - discussions, ratings, co-creation of ideas">Observational - Online qualitative - discussions, ratings, co-creation of ideas</option>
						<option value="Observational - FTF qualitative - discussions, ratings, co-creation of ideas">Observational - FTF qualitative - discussions, ratings, co-creation of ideas</option>
						<option value="Observational - Telephone qualitative - discussions, ratings, co-creation of ideas">Observational - Telephone qualitative - discussions, ratings, co-creation of ideas</option>
						<option value="Observational - other biometric (e.g. eye-tracking)">Observational - other biometric (e.g. eye-tracking)</option>
						<option value="Observational - Client observation">Observational - Client observation</option>
						<option value="Observational - qualitative transcripts">Observational - qualitative transcripts</option>
						<option value="Observational - qualitative videos or observation (e.g. camera recording in a living space)">Observational - qualitative videos or observation (e.g. camera recording in a living space)</option>
						<option value="Observational - Social Media monitoring - (e.g. Facebook, Twitter etc.)">Observational - Social Media monitoring - (e.g. Facebook, Twitter etc.)</option>
						<option value="Observational - wearable or other tracking device (e.g. Fitbit, Apple watch, or geolocator device etc.)">Observational - wearable or other tracking device (e.g. Fitbit, Apple watch, or geolocator device etc.)</option>
						<option value="Passive Monitoring - 3rd party meter">Passive Monitoring - 3rd party meter</option>
						<option value="Passive Monitoring - email and phone activity">Passive Monitoring - email and phone activity</option>
						<option value="Passive Monitoring - Kantar owned meter">Passive Monitoring - Kantar owned meter</option>
						<option value="Passive Monitoring - location">Passive Monitoring - location</option>
						<option value="Passive Monitoring - phone/network usage information">Passive Monitoring - phone/network usage information</option>
						<option value="Passive Monitoring - telemetry - keystroke tracking or other">Passive Monitoring - telemetry - keystroke tracking or other</option>
						<option value="Passive Monitoring - websites & apps visited">Passive Monitoring - websites & apps visited</option>
						<option value="Purchase tracking - bar code/survey capture">Purchase tracking - bar code/survey capture</option>
						<option value="Purchase Tracking - invoice capture (online and/or via photo)">Purchase Tracking - invoice capture (online and/or via photo)</option>
						<option value="Purchase Tracking - loyalty card data">Purchase Tracking - loyalty card data</option>
						<option value="Purchase Tracking - respondent supplied product photos">Purchase Tracking - respondent supplied product photos</option>
						<option value="Survey - chatbot or other AI">Survey - chatbot or other AI</option>
						<option value="Survey - offline, FTF (CAPI or PAPI)">Survey - offline, FTF (CAPI or PAPI)</option>
						<option value="Survey - offline, SMS/text">Survey - offline, SMS/text</option>
						<option value="Survey - offline, telephone">Survey - offline, telephone</option>
						<option value="Survey - online + offline blend">Survey - online + offline blend</option>
						<option value="Survey - online, client sample">Survey - online, client sample</option>
						<option value="Survey - online, 3rd party panel">Survey - online, 3rd party panel</option>
						<option value="Survey - online, 3rd party sample">Survey - online, 3rd party sample</option>
						<option value="Survey - online, blended panels">Survey - online, blended panels</option>
						<option value="Survey - online, Kantar owned panel">Survey - online, Kantar owned panel</option>
						<option value="Survey - purchased from 3rd party">Survey - purchased from 3rd party</option>
						<option value="3rd party data provision - respondent supplied (e.g. physician provide patient records, healthcare providors name KOLs/Payors, patients name influencers, carers provide patient information etc.)">3rd party data provision - respondent supplied (e.g. physician provide patient records, healthcare providors name KOLs/Payors, patients name influencers, carers provide patient information etc.)</option>
					</select>	
				</td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>