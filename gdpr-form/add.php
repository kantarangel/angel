<html>
<head>
	<title>Add Data</title>
</head>

<body>
<?php
//including the database connection file
include_once("config.php");

if(isset($_POST['Submit'])) {	
	$dateProject = mysqli_real_escape_string($mysqli, $_POST['dateProject']);
	$fieldworkDate = mysqli_real_escape_string($mysqli, $_POST['fieldworkDate']);
	$namePerson = mysqli_real_escape_string($mysqli, $_POST['namePerson']);
	$projectId = mysqli_real_escape_string($mysqli, $_POST['projectId']);
	$nameClient = mysqli_real_escape_string($mysqli, $_POST['nameClient']);
	$surveyMet = mysqli_real_escape_string($mysqli, $_POST['surveyMet']);
	$sampleSource = mysqli_real_escape_string($mysqli, $_POST['sampleSource']);
	$sampleReceived = mysqli_real_escape_string($mysqli, $_POST['sampleReceived']);
	$applicationsUsed = mysqli_real_escape_string($mysqli, $_POST['applicationsUsed']);
	$collectedFrom = mysqli_real_escape_string($mysqli, $_POST['collectedFrom']);
	$collectedType = mysqli_real_escape_string($mysqli, $_POST['collectedType']);
	$pseudo = mysqli_real_escape_string($mysqli, $_POST['pseudo']);
	$privacyPolicy = mysqli_real_escape_string($mysqli, $_POST['privacyPolicy']);
	$consent = mysqli_real_escape_string($mysqli, $_POST['consent']);
	$thirdYesNo = mysqli_real_escape_string($mysqli, $_POST['thirdYesNo']);
	$thirdParty = mysqli_real_escape_string($mysqli, $_POST['thirdParty']);
	$purposeSharing = mysqli_real_escape_string($mysqli, $_POST['purposeSharing']);
	$shareData = mysqli_real_escape_string($mysqli, $_POST['shareData']);
		
	// checking empty fields
	if(empty($dateProject) || empty($namePerson) || empty($projectId)) {
				
		if(empty($dateProject)) {
			echo "<font color='red'>Date of project field is empty.</font><br/>";
		}
		
		if(empty($namePerson)) {
			echo "<font color='red'>Name of person field is empty.</font><br/>";
		}
		
		if(empty($projectId)) {
			echo "<font color='red'>Project ID/reference field is empty.</font><br/>";
		}
		
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} else { 
		// if all the fields are filled (not empty) 
			
		//insert data to database	
		$result = mysqli_query($mysqli, "INSERT INTO users(dateProject,fieldworkDate,namePerson,projectId,nameClient,surveyMet,sampleSource,sampleReceived,applicationsUsed,collectedFrom,collectedType,pseudo,privacyPolicy,consent,thirdYesNo,thirdParty,purposeSharing,shareData) VALUES('$dateProject','$fieldworkDate','$namePerson','$projectId','$nameClient','$surveyMet','$sampleSource','$sampleReceived','$applicationsUsed','$collectedFrom','$collectedType','$pseudo','$privacyPolicy','$consent','$thirdYesNo','$thirdParty','$purposeSharing','$shareData')");
		
		//display success message
		echo "<font color='green'>Data added successfully.";
		echo "<br/><a href='index.php'>View Result</a>";
	}
}
?>
</body>
</html>
