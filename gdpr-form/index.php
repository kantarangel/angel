<?php
//including the database connection file
include_once("config.php");

//fetching data in descending order (lastest entry first)
//$result = mysql_query("SELECT * FROM users ORDER BY id DESC"); // mysql_query is deprecated
$result = mysqli_query($mysqli, "SELECT * FROM users ORDER BY id DESC"); // using mysqli_query instead
?>

<html>
<head>	
	<title>Kantar GDPR Form</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid content">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php">
          <img src="kantarlogo.png" width="120" alt="Kantar GDPR">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
</nav>
<a href="add.html"><button type="button" class="btn btn-primary">Add new data</button></a>

	<table class="table table-bordered table-responsive w-auto table-striped" width='100%' border=0>

	<tr>
		<th class="text-nowrap">Date of project completion</th>
		<th class="text-nowrap">Fieldwork dates</th>
		<th class="text-nowrap">Name of person completing checklist</th>
		<th class="text-nowrap">Project ID/reference</th>
		<th class="text-nowrap">Name of client</th>
		<th class="text-nowrap" style="padding-right:180px">Survey methodology</th>
		<th class="text-nowrap">Sample source</th>
		<th class="text-nowrap">Sample received</th>
		<th class="text-nowrap">Applications used</th>
		<th class="text-nowrap">Data collected from</th>
		<th class="text-nowrap">Data collected type</th>
		<th class="text-nowrap">Pseudonymized or Anonymized</th>
		<th class="text-nowrap">Privacy Policy</th>
		<th class="text-nowrap">Consent</th>
		<th class="text-nowrap">Name of third party</th>
		<th class="text-nowrap">Purpose of sharing</th>
		<th class="text-nowrap">Share personal data with other party</th>
		<th class="text-nowrap">Update</th>
	</tr>
	<?php 
	//while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
	while($res = mysqli_fetch_array($result)) { 		
		echo "<tr>";
		echo "<td>".$res['dateProject']."</td>";
		echo "<td>".$res['fieldworkDate']."</td>";
		echo "<td>".$res['namePerson']."</td>";	
		echo "<td>".$res['projectId']."</td>";
		echo "<td>".$res['nameClient']."</td>";
		echo "<td>".$res['surveyMet']."</td>";
		echo "<td>".$res['sampleSource']."</td>";
		echo "<td>".$res['sampleReceived']."</td>";
		echo "<td>".$res['applicationsUsed']."</td>";
		echo "<td>".$res['collectedFrom']."</td>";
		echo "<td>".$res['collectedType']."</td>";
		echo "<td>".$res['pseudo']."</td>";
		echo "<td>".$res['privacyPolicy']."</td>";
		echo "<td class='text-nowrap'>".$res['consent']."</td>";
		echo "<td>".$res['thirdParty']."</td>";
		echo "<td>".$res['purposeSharing']."</td>";
		echo "<td>".$res['shareData']."</td>";
		echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";		
		echo "</tr>";
	}
	?>
	</table>

</body>
</html>
